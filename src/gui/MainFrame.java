package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import interfaces.WorldListener;
import javafx.geometry.Rectangle2D;
import model.Box;
import model.Car;
import model.Heart;
import model.World;

public class MainFrame extends JFrame implements KeyListener, WorldListener {

	private GameCanvas gameCanvas;
	private World world;
	private JLabel labelSpeed;
	private static MainFrame frame;

	public static void main(String[] args) {
		zacatekHry();
	}

	public MainFrame(World world) {

		super();

		this.world = world;
		this.world.addListener(this);
		// Frame Config
		setTitle("GTA");
		setSize(1240, 980);
		setMinimumSize(new Dimension(550, 400));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		// setResizable(false);
		setLocationRelativeTo(null);
		setLayout(new BorderLayout());

		addKeyListener(this);

		labelSpeed = new JLabel("Rychlost: " + world.speed * 10 + " km/h");
		add(labelSpeed, BorderLayout.SOUTH);

		TimerTask timerTask = new TimerTask() {

			@Override
			public void run() {
				world.update();
				revalidate();
			}
		};
		gameCanvas = new GameCanvas(world, timerTask);
		add(gameCanvas, BorderLayout.CENTER);
		Timer timer = new Timer();
		timer.schedule(timerTask, 0, 100);

		setVisible(true);
	}

	public void revalidate() {
		super.revalidate();

		labelSpeed.setText("Rychlost: " + world.speed * 10 + " km/h" + "        HP : " + world.getCar().getHP()
				+ "     �as:" + world.getCas() + "  ujeto :" + world.ujeto);

		gameCanvas.repaint();

	}

	@Override
	public void keyPressed(KeyEvent e) {

		int key = e.getKeyCode();
		Car car = world.getCar();

		if (key == KeyEvent.VK_LEFT && car.getX() > 0) {
			car.updateX(-1f);
		}

		if (key == KeyEvent.VK_RIGHT && car.getX() < world.WIDTH - 4) {
			car.updateX(1f);
		}

		if (key == KeyEvent.VK_UP && world.speed < 20f) {
			// car.updateY(-2);
			world.speed += 1f;
		}

		if (key == KeyEvent.VK_DOWN && world.speed > 1f) {
			// car.updateY(2);
			world.speed -= 1f;
		}

	}

	public static void zacatekHry() {
		MainFrame stare = frame;
		String name = JOptionPane.showInputDialog("What's your name?");
		frame = new MainFrame(new World(new Car(name, 10F, 10F)));
		try {

			stare.setVisible(false);
			// stare.dispatchEvent(new WindowEvent(frame,
			// WindowEvent.WINDOW_CLOSING));
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCrashBox(Box box) {
		// System.out.println("CRASH with " + box.toString());
	}

	@Override
	public void onCatchHeart(Heart heart) {
		// System.out.println("CATCH heart " + heart.toString());
	}
}
