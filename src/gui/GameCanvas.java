package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import javafx.geometry.Rectangle2D;
import model.Box;
import model.Car;
import model.Heart;
import model.World;
import model.Zebricek;

public class GameCanvas extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private World world;

	public static final int ROAD_WIDTH = 500;
	BufferedImage tank, ork, srdce;
	TimerTask timerTask;

	public GameCanvas(World world, TimerTask timerTask) {
		super();
		this.timerTask = timerTask;
		try {

			tank = ImageIO.read(new File("tank.jpg"));
			ork = ImageIO.read(new File("ork.jpg"));
			srdce = ImageIO.read(new File("heart.png"));
		} catch (Exception e) {

		}

		int roadStartX = (getWidth() - ROAD_WIDTH) / 2;
		float ratioX = ROAD_WIDTH / World.WIDTH;
		float ratioY = getHeight() / World.HEIGHT;
		this.world = world;
		this.world.setRatiox(ratioX);
		this.world.setRatioy(ratioY);
	}

	public World getWorld() {
		return this.world;
	}

	@Override
	public void paint(Graphics g) {
		kolize();

		try {

			super.paint(g);
			int roadStartX = (getWidth() - ROAD_WIDTH) / 2;
			float ratioX = ROAD_WIDTH / World.WIDTH;
			float ratioY = getHeight() / World.HEIGHT;

			// 1. Draw grass
			g.setColor(new Color(100, 255, 100));
			g.fillRect(0, 0, getWidth(), getHeight());

			// 2. Draw road
			g.setColor(new Color(30, 30, 30));
			g.fillRect(roadStartX, 0, ROAD_WIDTH, getHeight());

			// 3. Draw BROWN boxes
			List<Box> boxes = world.getBoxes();
			// g.setColor(new Color(132, 96, 45));
			for (Box box : boxes) {
				g.fillRect((int) (roadStartX + (int) box.getX() * ratioX), (int) (box.getY() * ratioY),
						(int) (box.getWidth() * ratioX), (int) (box.getHeight() * ratioY));
				g.drawImage(ork, (int) (roadStartX + (int) box.getX() * ratioX), (int) (box.getY() * ratioY),
						(int) (box.getWidth() * ratioX), (int) (box.getHeight() * ratioY), null);
			}

			// 4. Draw RED hearts
			List<Heart> hearts = world.getHearts();
			// g.setColor(new Color(255, 0, 0));
			for (Heart heart : hearts) {
				g.drawImage(srdce, (int) (roadStartX + (int) heart.getX() * ratioX), (int) (heart.getY() * ratioY),
						(int) (heart.getWidth() * ratioX), (int) (heart.getHeight() * ratioY), null);
				// g.fillRect((int)(roadStartX + (int) heart.getX() * ratioX),
				// (int)
				// (heart.getY() * ratioY), (int) (heart.getWidth() * ratioX),
				// (int)
				// (heart.getHeight() * ratioY));
			}

			// 5. Draw White car
			try {

				// img = new TexturePaint(slate, new Rectangle(0, 0, 90, 60));

				g.drawImage(tank, (int) (roadStartX + (int) world.getCar().getX() * ratioX),
						getHeight() - (int) (world.getCar().getY() * ratioY),
						(int) (world.getCar().getWidth() * ratioX), (int) (world.getCar().getHeight() * ratioY), null);
			} catch (Exception e) {

			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void kontrolaHP() {
		if (world.getCar().getHP() == 0) {

			world.getCar().damage();
			konec();
		}

	}

	private void konec() {
		Zebricek.addScore(world.getCar().getName(), world.getCas(), world.ujeto);
		timerTask.cancel();
		JOptionPane.showMessageDialog(null, "jsi mrtvej a p�e�il jsi po dobu: " + world.getCas() + " vte�in");

		int dialogResult = JOptionPane.showConfirmDialog(null, "Chce hr�t znova?", "NEW GAME ?",
				JOptionPane.YES_NO_OPTION);
		if (dialogResult == JOptionPane.YES_OPTION) {
			MainFrame.zacatekHry();
		} else {
			JOptionPane.showMessageDialog(null, Zebricek.vypis());
			System.exit(0);
		}
	}

	private void kolize() {
		int roadStartX = (getWidth() - ROAD_WIDTH) / 2;
		float ratioX = ROAD_WIDTH / World.WIDTH;
		float ratioY = getHeight() / World.HEIGHT;

		Car car = world.getCar();
		try {

			List<Box> rem = new ArrayList<Box>();
			for (Box box : world.getBoxes()) {
				Rectangle2D r = new Rectangle2D((roadStartX + (int) box.getX() * ratioX), (int) (box.getY() * ratioY),
						(int) (box.getWidth() * ratioX), (int) (box.getHeight() * ratioY));
				Rectangle2D p = new Rectangle2D((int) (roadStartX + (int) car.getX() * ratioX),
						getHeight() - (int) (car.getY() * ratioY), (int) (car.getWidth() * ratioX),
						(int) (car.getHeight() * ratioY));

				// Assuming there is an intersect method, otherwise just
				// handcompare the values
				if (r.intersects(p)) {
					System.out.println("bum");
					world.getCar().damage();
					rem.add(box);
					kontrolaHP();

					// A Collision!
					// we know which enemy (e), so we can call e.DoCollision();
					// e.DoCollision();
				}
			}

			for (Box box : rem) {
				world.getBoxes().remove(box);
				double x = world.getRandomX();
				double y = 0;

				world.addBox(new Box((float) x, (float) y));
			}

			List<Heart> rem1 = new ArrayList<Heart>();
			for (Heart heart : world.getHearts()) {
				Rectangle2D r = new Rectangle2D((roadStartX + (int) heart.getX() * ratioX),
						(int) (heart.getY() * ratioY), (int) (heart.getWidth() * ratioX),
						(int) (heart.getHeight() * ratioY));
				Rectangle2D p = new Rectangle2D((int) (roadStartX + (int) car.getX() * ratioX),
						getHeight() - (int) (car.getY() * ratioY), (int) (car.getWidth() * ratioX),
						(int) (car.getHeight() * ratioY));

				// Assuming there is an intersect method, otherwise just
				// handcompare the values
				if (r.intersects(p)) {
					System.out.println("heal");
					world.getCar().hp();
					rem1.add(heart);
					// A Collision!
					// we know which enemy (e), so we can call e.DoCollision();
					// e.DoCollision();
				}
			}

			for (Heart heart : rem1) {
				world.getHearts().remove(heart);
				double x = world.getRandomX();
				double y = 0f;

				world.addHeart(new Heart((float) x, (float) y));
			}

		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}
