package model;

public class Score {
	private String jmeno;
	private long cas;
	private int vzdalenost;

	public Score(String jmeno, long cas, int vzdalenost) {
		super();
		this.jmeno = jmeno;
		this.cas = cas;
		this.vzdalenost = vzdalenost;
	}

	public String getJmeno() {
		return jmeno;
	}

	public void setJmeno(String jmeno) {
		this.jmeno = jmeno;
	}

	public long getCas() {
		return cas;
	}

	public void setCas(long cas) {
		this.cas = cas;
	}

	public int getVzdalenost() {
		return vzdalenost;
	}

	public void setVzdalenost(int vzdalenost) {
		this.vzdalenost = vzdalenost;
	}

	@Override
	public String toString() {
		return "Score [jmeno=" + jmeno + ", cas=" + cas + ", vzdalenost=" + vzdalenost + "]";
	}

}
