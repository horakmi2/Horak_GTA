package model;

import java.util.ArrayList;

public class Zebricek {

	private static ArrayList<Score> zebricek = new ArrayList<Score>();

	public static void addScore(String jmeno, long cas, int vzdalenost) {
		Score nove = new Score(jmeno, cas, vzdalenost);
		zebricek.add(nove);

		if (zebricek.size() >= 2)
			for (int i = zebricek.size() - 1; i >= 0; i--) {
				for (int j = 0; j < i; j++) {
					if (zebricek.get(j).getVzdalenost() < zebricek.get(j + 1).getVzdalenost()) {
						Score temp = zebricek.get(j);
						zebricek.set(j, zebricek.get(j + 1));
						zebricek.set(j + 1, temp);
					}
				}
			}

	}

	public static String vypis() {

		String vypis = "";
		for (int i = 0; i < 10; i++) {
			try {
				vypis += zebricek.get(i).toString() + "\n";
			} catch (Exception e) {
				// TODO: handle exception
			}

		}
		return vypis;

	}
}
