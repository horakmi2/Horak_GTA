package model;

import java.util.*;

import interfaces.WorldListener;
import javafx.geometry.Rectangle2D;

public class World {

	public static final float WIDTH = 15f;
	public static final float HEIGHT = 100f;
	private long cas;

	public long getCas() {
		return cas;
	}

	private Date date1;
	private Date date2;
	public int ujeto;
	private Car car;

	private List<Heart> hearts;
	private List<Box> boxes;

	public float speed = 1f;

	public World(Car car) {
		this.car = car;
		ujeto = 0;
		this.hearts = new ArrayList<Heart>();
		this.boxes = new ArrayList<Box>();
		date1 = new Date();

		init();
	}

	public void addBox(Box box) {
		boxes.add(box);
	}

	public void addHeart(Heart heart) {
		hearts.add(heart);
	}

	public void init() {

		for (int i = 0; i < 5; i++) {
			double x = getRandomX();
			double y = 20 + (double) getRandomY();

			addBox(new Box((float) x, (float) y));
		}

		for (int i = 0; i < 1; i++) {
			double x = getRandomX();
			double y = 50 + (double) getRandomY();

			addHeart(new Heart((float) x, (float) y));
		}

	}

	public void update() {
		date2 = new Date();
		cas = (date2.getTime() - date1.getTime()) / 1000;
		ujeto += speed;
		for (Box box : boxes) {
			box.y += speed;
			if (box.y > HEIGHT + 30) {
				box.y = 0f;
				box.x = getRandomX();
			}
			if (car.collidesWith(box.getRectangle())) {
				// TODO SEBRAT SKORE
				// box.x = -100;
				// worldListener.onCrashBox(box);
			}
		}
		for (Heart heart : hearts) {
			heart.y += speed;
			if (heart.y > HEIGHT + 30) {
				heart.y = 0f;
				heart.x = getRandomX();
			}
			if (car.collidesWith(heart.getRectangle())) {
				// TODO SEBRAT SKORE
				// worldListener.onCatchHeart(heart);
			}
		}
	}

	public int getUjeto() {
		return ujeto;
	}

	public float getRandomX() {
		return (float) Math.random() * World.WIDTH - 1;
	}

	public float getRandomY() {
		return (float) Math.random() * World.HEIGHT / 3;
	}

	public Car getCar() {
		return this.car;
	}

	public List<Heart> getHearts() {
		return this.hearts;
	}

	public List<Box> getBoxes() {
		return this.boxes;
	}

	public void setRatiox(float ratiox) {
	}

	public void setRatioy(float ratioy) {
	}

	public void addListener(WorldListener worldListener) {
	}

	@Override
	public String toString() {
		String q = "World: \n";

		q += car + "\n";

		for (int i = 0; i < hearts.size(); i++) {
			q += hearts.get(i) + "\n";
		}
		for (int i = 0; i < boxes.size(); i++) {
			q += boxes.get(i) + "\n";
		}

		return q;
	}
}
