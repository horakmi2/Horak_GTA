package model;

import java.awt.geom.Rectangle2D;

public class Box extends GameObject {
	private float width;
	private float height;

	private Rectangle2D.Float rectangle;

	public Box(float x, float y) {
		this(1, x, y);
	}

	public Box(float width, float x, float y) {
		super(x, y);
		this.width = 3f;
		this.height = 3f;
		rectangle = new Rectangle2D.Float(x, y, width, height);
	}

	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public Rectangle2D.Float getRectangle() {
		this.rectangle.x = y;
		this.rectangle.y = y;
		return this.rectangle;
	}

	@Override
	public String toString() {
		String q = "Box: ";
		q += "width: " + width + "   pos: " + x + " " + y + "\n";
		return q;
	}

}
