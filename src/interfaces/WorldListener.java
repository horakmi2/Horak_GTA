package interfaces;

import model.Box;
import model.Heart;

public interface WorldListener {

	public void onCrashBox(Box box);

	public void onCatchHeart(Heart heart);

}
